import setuptools
import os

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()


def read(rel_path: str) -> str:
    here = os.path.abspath(os.path.dirname(__file__))
    # intentionally *not* adding an encoding option to open, See:
    #   https://github.com/pypa/virtualenv/issues/201#issuecomment-3145690
    with open(os.path.join(here, rel_path)) as fp:
        return fp.read()


def get_version(rel_path: str) -> str:
    for line in read(rel_path).splitlines():
        if line.startswith("__version__"):
            delim = '"' if '"' in line else "'"
            return line.split(delim)[1]
    raise RuntimeError("Unable to find version string.")


setuptools.setup(
    name="do_artifactory",
    version=get_version("do_artifactory/__init__.py"),
    author="Daniel Oberti",
    author_email="obertidaniel@gmail.com",
    description="Artifactory API",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/doberti/do_artifactory",
    download_url='http://pypi.python.org/pypi/do_artifactory/',
    license="MIT",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent"
    ],
    packages=["do_artifactory"],
    platforms='any',
    data_files=[
        ('share/do_artifactory/examples', ['examples/ejemplo1.py']),
        ('share/do_artifactory', ['LICENSE', 'README.md']),
    ],
    entry_points={
        'console_scripts': ['do_artifactory=do_artifactory.do_artifactory:main'] #????
    },
    python_requires=">=3.7",
    # setup_requires=['flake8']
    install_requires=[
        'dohq-artifactory',
        'tabulate',
        'redis',
        'pandas',
        'PyYAML',
        'requests',
        'urllib3'
    ]
)
